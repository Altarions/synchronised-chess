import { createStore } from 'vuex'

// Create a new store instance
const store = createStore({
    state() {
        return {
        }
    },
    // ways to change our state (must by synchronous) store.commit("SET_USER", user)
    mutations: {
    },
    // ways to call mutations (can be asynchronous) store.dispatch("setUser", user)
    actions: {
    },
    getters: {
    }
});

export default store;