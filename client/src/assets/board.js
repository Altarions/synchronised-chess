
const SIZE = 0.8;

export class Board {

	constructor(canvasId, sendMove) {

		this.sendMove = sendMove;

		this.canvas = document.getElementById(canvasId);
		this.ctx = this.canvas.getContext("2d");

		this.maxSize = Math.min(window.innerWidth, window.innerHeight);

	  	this.canvasSize = Math.floor(SIZE * this.maxSize);
		this.squareSize = this.canvasSize / 8;

		this.canvas.width = this.canvasSize;
		this.canvas.height = this.canvasSize;

		this.centerX = (window.innerWidth - this.canvasSize) / 2;
		this.centerY = (window.innerHeight - this.canvasSize) / 2;

		this.canvas.style.position = "absolute";
		this.canvas.style.left = this.centerX + "px";
		this.canvas.style.top = this.centerY + "px";

		this.ctx.fillStyle = "lightgray";
		this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

		this.currentPlayer = 'WHITE';
		this.selectedCase = null;
		this.status = "CONNECTING";

		this.tileColor = {
			white: "#f0d9b5",
			black: "#b58863"
		}

		this.selectedTileColor = {
			white: "#646e40",
			black: "#829769"
		}

		this.pieceSize = 0.8;

		this.pieces = [
			{col:0, row:0, piece:"♖", color:"WHITE"},
			{col:1, row:0, piece:"♘", color:"WHITE"},
			{col:2, row:0, piece:"♗", color:"WHITE"},
			{col:3, row:0, piece:"♕", color:"WHITE"},
			{col:4, row:0, piece:"♔", color:"WHITE"},
			{col:5, row:0, piece:"♗", color:"WHITE"},
			{col:6, row:0, piece:"♘", color:"WHITE"},
			{col:7, row:0, piece:"♖", color:"WHITE"},
			{col:0, row:7, piece:"♜", color:"BLACK"},
			{col:1, row:7, piece:"♞", color:"BLACK"},
			{col:2, row:7, piece:"♝", color:"BLACK"},
			{col:3, row:7, piece:"♚", color:"BLACK"},
			{col:4, row:7, piece:"♛", color:"BLACK"},
			{col:5, row:7, piece:"♝", color:"BLACK"},
			{col:6, row:7, piece:"♞", color:"BLACK"},
			{col:7, row:7, piece:"♜", color:"BLACK"},
			{col:2, row:5, piece: "♙", color:"WHITE"},
			{col:6, row:3, piece: "♟", color:"BLACK"},
		];

		// this.canvas.addEventListener('click', this.handleMouseClick.bind(this));

		this.update();
	}

	update() {
		switch(this.status) {
			case "CONNECTING":
				this.drawChessboard();
				this.printFullScreenText("CONNECTING", null, "white", this.squareSize, "black", 3);
				break;
			case "WAITING":
				this.drawChessboard();
				this.printFullScreenText("WAITING", null, "white", this.squareSize, "black", 3);
				break;
			case "IN_PROGRESS":
				this.drawChessboard();
				this.drawCoordinates(this.currentPlayer);
				this.drawPieces(this.currentPlayer);
				break;
			case "WHITE_WIN":
				this.printFullScreenText("WHITE WON", null, "white", this.squareSize, "black", 3);
				break;
			case "BLACK_WIN":
				this.printFullScreenText("BLACK WON", null, "white", this.squareSize, "black", 3);
				break;
			case "SPECTATING":
				this.drawChessboard();
				this.drawCoordinates(this.currentPlayer);
				this.drawPieces(this.currentPlayer);
				this.printFullScreenText("SPECTATING", null, "white", this.squareSize/2, "black", 2);
				break;
		}
		
	}

	printFullScreenText(text, backgroundColor, textColor, textSize, textStroke, strokeSize) {
		// background
		if(backgroundColor != null) {
			this.ctx.fillStyle = backgroundColor;
			this.ctx.fillRect(0, 0, this.canvasSize, this.canvasSize);
		}
		

		// text
		this.ctx.font = textSize + "px Arial";
		this.ctx.lineWidth = strokeSize;
		this.ctx.textAlign='center';
		this.ctx.textBaseline = "bottom";
		this.ctx.fillStyle = textColor;
		this.ctx.strokeStyle = textStroke;
		this.ctx.fillText(text, this.canvasSize/2, this.canvasSize/2);
		this.ctx.strokeText(text, this.canvasSize/2, this.canvasSize/2);
	}

	changeSide() {
		this.currentPlayer = (this.currentPlayer === 'WHITE') ? 'BLACK' : 'WHITE';
		this.update();
	}

	drawChessboard() {
		for (let i = 0; i < 8; i++) {
			for (let j = 0; j < 8; j++) {
		  		const x = i * this.squareSize;
		  		const y = j * this.squareSize;
				const isWhite = (i + j) % 2 === 0;
				this.ctx.fillStyle = isWhite ? this.tileColor.white : this.tileColor.black;
				this.ctx.fillRect(x, y, this.squareSize, this.squareSize);
			}
		}

		if (this.selectedCase !== null) {
			const x = (this.currentPlayer === 'WHITE') ? this.selectedCase.col * this.squareSize : (7 - this.selectedCase.col) * this.squareSize;
			const y = (this.currentPlayer === 'WHITE') ? (7 - this.selectedCase.row) * this.squareSize : this.selectedCase.row * this.squareSize;
			const isWhite = (this.selectedCase.col + this.selectedCase.row) % 2 === 0;

			this.ctx.fillStyle = isWhite ? this.selectedTileColor.white : this.selectedTileColor.black;
			this.ctx.fillRect(x, y, this.squareSize, this.squareSize);
		}
	}

	drawCoordinates(currentPlayer) {
		let letters = ["a","b","c","d","e","f","g","h"];
		const spacing = this.squareSize/16;
		this.ctx.font = this.squareSize/8 + "px Arial";
		this.ctx.lineWidth = 1;

		for(let i = 0; i < 8; i++) {
			// letters
			this.ctx.textAlign='left';
			this.ctx.textBaseline = "bottom";
			this.ctx.fillStyle = (i%2 === 0) ? this.tileColor.white : this.tileColor.black;
			const letter = (this.currentPlayer === "WHITE") ? letters[i] : letters[7-i]
			this.ctx.fillText(letter, spacing + i*this.squareSize, 8*this.squareSize-spacing);

			  // numbers
			this.ctx.textAlign='right';
			this.ctx.textBaseline = "top";
			this.ctx.fillStyle = (i%2 === 1) ? this.tileColor.white : this.tileColor.black;
			const number = (currentPlayer === "WHITE") ? i+1 : 8-i;
			this.ctx.fillText(number, 8*this.squareSize-spacing, this.squareSize*8 - (this.squareSize-spacing) - i*this.squareSize);
		}
	}

	drawPieces(currentPlayer) {
		this.ctx.textAlign='center';
		this.ctx.textBaseline = "middle";
		this.ctx.fillStyle = "black";
		this.ctx.font = this.squareSize*this.pieceSize + "px Arial";
		this.pieces.forEach((piece) => 
			this.ctx.fillText(piece.piece,
				(this.currentPlayer === "WHITE") ? piece.col*this.squareSize + this.squareSize/2 : this.squareSize*8 - piece.col*this.squareSize - this.squareSize/2,
				(this.currentPlayer === "WHITE") ? this.squareSize*8 - piece.row*this.squareSize - this.squareSize/2 : piece.row*this.squareSize + this.squareSize/2
			)
		);
	}

	updatePieces(pieces) {
		this.pieces = pieces;

		// if the board updates and the previously selected case is no longer one of your pawns it's unselected
		if(this.selectedcase != null) {
			this.pieces.forEach( (p) => {
				if(p.row == this.selectedCase.row && p.col == this.selectedCase.col) {
					this.selected = (p.color == this.currentPlayer) ? this.selected : null;
				}
			});
		}
		this.update();
	}

	handleMouseClick(event) {
		if(this.status == "IN_PROGRESS") {
			const rect = this.canvas.getBoundingClientRect();
			const x = event.clientX - rect.left;
			const y = event.clientY - rect.top;

			const row = (this.currentPlayer === "WHITE") ? 7-Math.floor(y / this.squareSize) : Math.floor(y / this.squareSize);
			const col = (this.currentPlayer === "WHITE") ? Math.floor(x / this.squareSize) : 7-Math.floor(x / this.squareSize);

			console.log("click col " + col + " row " + row + " " + this.currentPlayer);

			let selectedOwnPiece = false;
			this.pieces.forEach((piece) => {
				console.log("piece col " + piece.col + " row " + piece.row + " " + piece.color);
				selectedOwnPiece = (piece.col === col && piece.row === row && piece.color === this.currentPlayer) ? true : selectedOwnPiece
			});
			
			// console.log(selectedCase);
			
			if(this.selectedCase === null) {
				this.selectedCase = (selectedOwnPiece) ? {row: row, col: col} : null;
			} else {
				// send move
				console.log(`attack from (col ${this.selectedCase.col}, row ${this.selectedCase.row}), to (col ${col}, row ${row})`)

				this.sendMove({from: {col: this.selectedCase.col, row: this.selectedCase.row}, to: {col: col, row: row}});

				this.selectedCase = null;
			}
			//console.log(`Clicked on row ${row}, column ${col}, Current player: ${currentPlayer}`);
			this.update();
		}
		
	}
}