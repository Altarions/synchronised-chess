# Synchronised Chess 

## Description

This is a chess game which is not played turn by turn, instead each player can play as fast as it wants.
The rule is simple, eat the opponent's king.

## Concurency

The main concurency point of this project is the players movement and retrieving the board.
Each player may try moving at the same time but each action should be done in the right order, since a player action may have an impact on the other players move.
Also, reading the board state shouldn't happen while a move is being done.

For that we implemented a BoardBuffer that can stack up Commands in a Blocking Queue, a command is either a move or a reading of the board (MoveCommand / GetPiecesListCommand).
By doing so, each player can spam as much actions as possible at the same time, the events shouldn't interfere with each others.

## Structure

The current project structure is split between a server and a client. The client is developed using the [Vue 3](https://vuejs.org) framework, while the server is based on [Spring Boot](https://spring.io/projects/spring-boot/). Communication between the client and server is established through the use of *WebSockets*, thus ensuring a two-way, real-time connection between the two components. To guarantee data consistency between the client and the server, the project integrates a buffer mechanism. This buffer acts as a temporary storage area where data can be held before being synchronized between the client and server.

Below the project tree, with the most important files :
```
.
|-- client/
|   |-- src/
|       |-- components/
|       |   |-- Board.vue
|       |
|       |-- views/
|       |   |-- Game.vue
|       |
|       |-- assets/
|           |--board.js
|
|-- server/
|   |-- src/
|       |-- main/
|       |   |-- java/en/
|       |       |-- socket/
|       |       |   |--controller/
|       |       |   |   |--BoardController.java
|       |       |   |
|       |       |   |--models/
|       |       |   |   |--BoardResponse.java
|       |       |   |   
|       |       |   |--HandshakerHandler.java
|       |       |   |--WebsocketConfig.java
|       |       |
|       |       |-- synchronisedchess/
|       |       |   |--board/
|       |       |       |-- BoardBuffer.java
|       |       |
|       |       |--Server.java
|       |
|       |-- test/
|
|
|-- README.md
|-- .gitignore
|-- LICENSE

```

## How to run the project

You need to have installed :
- maven
- npm
- vite

### BackEnd

For the backend, go into the "server" folder.

First initialise the project with the command

```mvn install```

You can launch the server by running the command

```mvn spring-boot:run```


You can run the Tests by running the command

```mvn clean test```

### FrontEnd

For the frontend, go into the "client" folder.

First initialise the project with the command

```npm i```

You can launch the frontend by running the command

```npm run dev```


## Playing the Game

Once the Backend and the Frontend are launched, you can open two windows of the link given by the Frontend (it should be http://localhost:5173/).
And you can immediately play on both windows.

> :warning:  
> The game is currently made in a way that the first 2 people connecting to it are the players, any more player connecting are spectators.
> If one of the 2 playing windows are closed, the game can't be played and you must relaunch the backend by killing its execution and launching it again with the command provided above.

## Images

The inital window showing the attempt to connect to the Backend, if you're stuck on this, make sure the Backend is launched.

![Connection Window](/assets/connection.png)

Once the connection is established you're on the waiting screen for a second opponent to connect.

![Waiting Screen](/assets/waiting.png)

Once a second player is connected the game immediately start and both player can make their moves as fast as possible to win.

![Playing Chess](/assets/gameStarted.png)

If a third person open the website, they will be spectating the game.

![Spectating Screen](/assets/spectating.png)

Once a player eats his opponent's king, he win, the game will be stopped and a new one will be started 5 seconds later with the same players.

![Winning Screen](/assets/wining.png)


## Contributors

This project was made by :

- GARNIER Cypien
- BROCHARD Julien

For the "Middleware" project of Master 2 ALMA at Nantes University.
