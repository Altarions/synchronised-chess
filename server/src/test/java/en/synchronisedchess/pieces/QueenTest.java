package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InvalidAttributeValueException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class QueenTest {
    private final BoardManager board = new BoardManager();
    private final Position queenWhitePos = new Position(3,4);;
    private Queen queen;

    @BeforeEach
    void setup() throws InvalidAttributeValueException {
        board.cleanBoard();
        queen = new Queen(Color.WHITE, board);
        board.addPieceInBoard(queen, queenWhitePos);
    }

    @Test
    public void canMove() {
        Position moveTo1 = new Position(3,0);
        Position moveTo2 = new Position(0,4);
        Position moveTo3 = new Position(7,4);
        Position moveTo4 = new Position(3,7);

        Position moveTo5 = new Position(6,7);
        Position moveTo6 = new Position(2,3);
        Position moveTo7 = new Position(2,5);
        Position moveTo8 = new Position(7,0);

        assertTrue(queen.canMove(new Move(queenWhitePos,moveTo1)));
        assertTrue(queen.canMove(new Move(queenWhitePos,moveTo2)));
        assertTrue(queen.canMove(new Move(queenWhitePos,moveTo3)));
        assertTrue(queen.canMove(new Move(queenWhitePos,moveTo4)));

        assertTrue(queen.canMove(new Move(queenWhitePos,moveTo5)));
        assertTrue(queen.canMove(new Move(queenWhitePos,moveTo6)));
        assertTrue(queen.canMove(new Move(queenWhitePos,moveTo7)));
        assertTrue(queen.canMove(new Move(queenWhitePos,moveTo8)));
    }

    @Test
    public void cantMove(){
        Position moveTo1 = new Position(2,2);
        Position moveTo2 = new Position(0,6);
        Position moveTo3 = new Position(6,0);
        Position moveTo4 = new Position(4,6);

        assertFalse(queen.canMove(new Move(queenWhitePos,moveTo1)));
        assertFalse(queen.canMove(new Move(queenWhitePos,moveTo2)));
        assertFalse(queen.canMove(new Move(queenWhitePos,moveTo3)));
        assertFalse(queen.canMove(new Move(queenWhitePos,moveTo4)));
    }

    @Test
    public void canTakePiece() throws InvalidAttributeValueException {
        Pawn backPawn = new Pawn(Color.BLACK,board);
        Position blackPawnPos = new Position(2,4);

        board.addPieceInBoard(backPawn,blackPawnPos);

        assertTrue(queen.canMove(new Move(queenWhitePos,blackPawnPos)));
    }

    @Test
    public void cantTakeAllyPiece() throws InvalidAttributeValueException {
        Pawn whitePawn = new Pawn(Color.WHITE,board);
        Position whitePawnPos = new Position(7,4);

        board.addPieceInBoard(whitePawn,whitePawnPos);

        assertFalse(queen.canMove(new Move(queenWhitePos,whitePawnPos)));
    }

    @Test
    public void cantTakePieceWithUnclearPath() throws InvalidAttributeValueException {
        Pawn backPawn1 = new Pawn(Color.BLACK,board);
        Pawn backPawn2 = new Pawn(Color.BLACK,board);
        Position blackPawnPos1 = new Position(3,0);
        Position blackPawnPos2 = new Position(3,2);

        board.addPieceInBoard(backPawn1,blackPawnPos1);
        board.addPieceInBoard(backPawn2,blackPawnPos2);

        assertFalse(queen.canMove(new Move(queenWhitePos,blackPawnPos1)));
    }
}
