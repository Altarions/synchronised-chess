package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InvalidAttributeValueException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BishopTest {
    private final BoardManager board = new BoardManager();
    private final Position bishopWhitePos = new Position(3,4);;
    private Bishop bishop;

    @BeforeEach
    void setup() throws InvalidAttributeValueException {
        board.cleanBoard();
        bishop = new Bishop(Color.WHITE, board);
        board.addPieceInBoard(bishop, bishopWhitePos);
    }

    @Test
    public void canMove() {
        Position moveTo1 = new Position(6,7);
        Position moveTo2 = new Position(2,3);
        Position moveTo3 = new Position(2,5);
        Position moveTo4 = new Position(7,0);

        assertTrue(bishop.canMove(new Move(bishopWhitePos,moveTo1)));
        assertTrue(bishop.canMove(new Move(bishopWhitePos,moveTo2)));
        assertTrue(bishop.canMove(new Move(bishopWhitePos,moveTo3)));
        assertTrue(bishop.canMove(new Move(bishopWhitePos,moveTo4)));
    }

    @Test
    public void cantMove(){
        Position moveTo1 = new Position(0,6);
        Position moveTo2 = new Position(7,6);
        Position moveTo3 = new Position(3,3);

        assertFalse(bishop.canMove(new Move(bishopWhitePos,moveTo1)));
        assertFalse(bishop.canMove(new Move(bishopWhitePos,moveTo2)));
        assertFalse(bishop.canMove(new Move(bishopWhitePos,moveTo3)));
    }

    @Test
    public void canTakePiece() throws InvalidAttributeValueException {
        Pawn blackPawn = new Pawn(Color.BLACK,board);
        Position blackPawnPos = new Position(1,2);

        board.addPieceInBoard(blackPawn,blackPawnPos);

        assertTrue(bishop.canMove(new Move(bishopWhitePos,blackPawnPos)));
    }

    @Test
    public void cantTakeAllyPiece() throws InvalidAttributeValueException {
        Pawn whitePawn = new Pawn(Color.WHITE,board);
        Position whitePawnPos = new Position(1,6);

        board.addPieceInBoard(whitePawn,whitePawnPos);

        assertFalse(bishop.canMove(new Move(bishopWhitePos,whitePawnPos)));
    }

    @Test
    public void cantTakePieceWithUnclearPath() throws InvalidAttributeValueException {
        Pawn backPawn1 = new Pawn(Color.BLACK,board);
        Pawn backPawn2 = new Pawn(Color.BLACK,board);
        Position blackPawnPos1 = new Position(1,6);
        Position blackPawnPos2 = new Position(2,5);

        board.addPieceInBoard(backPawn1,blackPawnPos1);
        board.addPieceInBoard(backPawn2,blackPawnPos2);

        assertFalse(bishop.canMove(new Move(bishopWhitePos,blackPawnPos1)));
    }
}
