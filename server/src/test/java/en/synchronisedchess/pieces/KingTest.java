package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InvalidAttributeValueException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KingTest {

    private final BoardManager board = new BoardManager();
    private final Position kingWhitePos = new Position(3,4);;
    private King king;

    @BeforeEach
    void setup() throws InvalidAttributeValueException {
        board.cleanBoard();
        king = new King(Color.WHITE, board);
        board.addPieceInBoard(king,kingWhitePos);
    }

    @Test
    public void canMove() {
        Position moveTo1 = new Position(3,5);
        Position moveTo2 = new Position(4,5);
        Position moveTo3 = new Position(4,4);
        Position moveTo4 = new Position(2,3);

        assertTrue(king.canMove(new Move(kingWhitePos,moveTo1)));
        assertTrue(king.canMove(new Move(kingWhitePos,moveTo2)));
        assertTrue(king.canMove(new Move(kingWhitePos,moveTo3)));
        assertTrue(king.canMove(new Move(kingWhitePos,moveTo4)));
    }

    @Test
    public void cantMove(){
        Position moveTo1 = new Position(6,1);
        Position moveTo2 = new Position(4,7);
        Position moveTo3 = new Position(3,2);

        assertFalse(king.canMove(new Move(kingWhitePos,moveTo1)));
        assertFalse(king.canMove(new Move(kingWhitePos,moveTo2)));
        assertFalse(king.canMove(new Move(kingWhitePos,moveTo3)));
    }

    @Test
    public void canTakePiece() throws InvalidAttributeValueException {
        Pawn blackPawn = new Pawn(Color.BLACK,board);
        Position blackPawnPos = new Position(2,4);

        board.addPieceInBoard(blackPawn,blackPawnPos);

        assertTrue(king.canMove(new Move(kingWhitePos,blackPawnPos)));
    }

    @Test
    public void cantTakeAllyPiece() throws InvalidAttributeValueException {
        Pawn whitePawn = new Pawn(Color.WHITE,board);
        Position whitePawnPos = new Position(2,4);

        board.addPieceInBoard(whitePawn,whitePawnPos);

        assertFalse(king.canMove(new Move(kingWhitePos,whitePawnPos)));
    }

    @Test
    public void cantTakePieceTooFar() throws InvalidAttributeValueException {
        Pawn blackPawn = new Pawn(Color.BLACK,board);
        Position blackPawnPos = new Position(5,7);

        board.addPieceInBoard(blackPawn,blackPawnPos);

        assertFalse(king.canMove(new Move(kingWhitePos,blackPawnPos)));
    }
}
