package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InvalidAttributeValueException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KnightTest {
    private final BoardManager board = new BoardManager();
    private final Position knightWhitePos = new Position(3,4);;
    private Knight knight;

    @BeforeEach
    void setup() throws InvalidAttributeValueException {
        board.cleanBoard();
        knight = new Knight(Color.WHITE, board);
        board.addPieceInBoard(knight, knightWhitePos);
    }

    @Test
    public void canMove() {
        Position moveTo1 = new Position(2,6);
        Position moveTo2 = new Position(4,6);
        Position moveTo3 = new Position(5,5);
        Position moveTo4 = new Position(5,3);
        Position moveTo5 = new Position(4,2);
        Position moveTo6 = new Position(2,2);
        Position moveTo7 = new Position(1,3);
        Position moveTo8 = new Position(1,5);

        assertTrue(knight.canMove(new Move(knightWhitePos,moveTo1)));
        assertTrue(knight.canMove(new Move(knightWhitePos,moveTo2)));
        assertTrue(knight.canMove(new Move(knightWhitePos,moveTo3)));
        assertTrue(knight.canMove(new Move(knightWhitePos,moveTo4)));
        assertTrue(knight.canMove(new Move(knightWhitePos,moveTo5)));
        assertTrue(knight.canMove(new Move(knightWhitePos,moveTo6)));
        assertTrue(knight.canMove(new Move(knightWhitePos,moveTo7)));
        assertTrue(knight.canMove(new Move(knightWhitePos,moveTo8)));
    }

    @Test
    public void cantMove(){
        Position moveTo1 = new Position(0,6);
        Position moveTo2 = new Position(6,7);
        Position moveTo3 = new Position(2,3);

        assertFalse(knight.canMove(new Move(knightWhitePos,moveTo1)));
        assertFalse(knight.canMove(new Move(knightWhitePos,moveTo2)));
        assertFalse(knight.canMove(new Move(knightWhitePos,moveTo3)));
    }

    @Test
    public void cantTakeAllyPiece() throws InvalidAttributeValueException {
        Pawn whitePawn = new Pawn(Color.WHITE,board);
        Position whitePawnPos = new Position(2,6);

        board.addPieceInBoard(whitePawn,whitePawnPos);

        assertFalse(knight.canMove(new Move(knightWhitePos,whitePawnPos)));
    }
}
