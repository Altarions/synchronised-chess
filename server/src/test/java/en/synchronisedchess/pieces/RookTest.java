package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InvalidAttributeValueException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RookTest {
    private final BoardManager board = new BoardManager();
    private final Position rookWhitePos = new Position(3,4);;
    private Rook rook;

    @BeforeEach
    void setup() throws InvalidAttributeValueException {
        board.cleanBoard();
        rook = new Rook(Color.WHITE, board);
        board.addPieceInBoard(rook, rookWhitePos);
    }

    @Test
    public void canMove() {
        Position moveTo1 = new Position(3,0);
        Position moveTo2 = new Position(0,4);
        Position moveTo3 = new Position(7,4);
        Position moveTo4 = new Position(3,7);

        assertTrue(rook.canMove(new Move(rookWhitePos,moveTo1)));
        assertTrue(rook.canMove(new Move(rookWhitePos,moveTo2)));
        assertTrue(rook.canMove(new Move(rookWhitePos,moveTo3)));
        assertTrue(rook.canMove(new Move(rookWhitePos,moveTo4)));
    }

    @Test
    public void cantMove(){
        Position moveTo1 = new Position(0,6);
        Position moveTo2 = new Position(6,7);
        Position moveTo3 = new Position(2,3);

        assertFalse(rook.canMove(new Move(rookWhitePos,moveTo1)));
        assertFalse(rook.canMove(new Move(rookWhitePos,moveTo2)));
        assertFalse(rook.canMove(new Move(rookWhitePos,moveTo3)));
    }

    @Test
    public void canTakePiece() throws InvalidAttributeValueException {
        Pawn backPawn = new Pawn(Color.BLACK,board);
        Position blackPawnPos = new Position(2,4);

        board.addPieceInBoard(backPawn,blackPawnPos);

        assertTrue(rook.canMove(new Move(rookWhitePos,blackPawnPos)));
    }

    @Test
    public void cantTakeAllyPiece() throws InvalidAttributeValueException {
        Pawn whitePawn = new Pawn(Color.WHITE,board);
        Position whitePawnPos = new Position(7,4);

        board.addPieceInBoard(whitePawn,whitePawnPos);

        assertFalse(rook.canMove(new Move(rookWhitePos,whitePawnPos)));
    }

    @Test
    public void cantTakePieceWithUnclearPath() throws InvalidAttributeValueException {
        Pawn backPawn1 = new Pawn(Color.BLACK,board);
        Pawn backPawn2 = new Pawn(Color.BLACK,board);
        Position blackPawnPos1 = new Position(3,0);
        Position blackPawnPos2 = new Position(3,2);

        board.addPieceInBoard(backPawn1,blackPawnPos1);
        board.addPieceInBoard(backPawn2,blackPawnPos2);

        assertFalse(rook.canMove(new Move(rookWhitePos,blackPawnPos1)));
    }
}
