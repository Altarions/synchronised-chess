package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;
import org.junit.jupiter.api.Test;

import javax.management.InvalidAttributeValueException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PawnTest {
    private final BoardManager board = new BoardManager();

    @Test
    public void canMove(){
        Position pawnWhitePos = new Position(4,1);
        Piece pawn = board.getPieceFrom(pawnWhitePos);

        Position moveTo1 = new Position(4,2);
        Position moveTo2 = new Position(4,3);

        assertTrue(pawn.canMove(new Move(pawnWhitePos,moveTo1)));
        assertTrue(pawn.canMove(new Move(pawnWhitePos,moveTo2)));
    }

    @Test
    public void cantMove() throws InvalidAttributeValueException {
        Position pawnWhitePos = new Position(4,1);
        Position posBlack = new Position(4,2);
        Piece pawn = board.getPieceFrom(pawnWhitePos);
        Pawn blackPawn = new Pawn(Color.BLACK,board);

        board.addPieceInBoard(blackPawn, posBlack);
        Position moveTo1 = new Position(6,1);
        Position moveTo2 = new Position(4,7);
        Position moveTo3 = new Position(4,4);
        Position moveTo4 = new Position(4,3);

        assertFalse(pawn.canMove(new Move(pawnWhitePos,moveTo1)));
        assertFalse(pawn.canMove(new Move(pawnWhitePos,moveTo2)));
        assertFalse(pawn.canMove(new Move(pawnWhitePos,moveTo3)));
        assertFalse(pawn.canMove(new Move(pawnWhitePos,moveTo4)));
    }

    @Test
    public void canTakePiece() throws InvalidAttributeValueException {
        Position pawnWhitePos = new Position(4,1);
        Position posBlack1 = new Position(3,2);
        Position posBlack2 = new Position(5,2);
        Piece pawn = board.getPieceFrom(pawnWhitePos);
        Pawn blackPawn1 = new Pawn(Color.BLACK,board);
        Pawn blackPawn2 = new Pawn(Color.BLACK,board);

        board.addPieceInBoard(blackPawn1, posBlack1);
        board.addPieceInBoard(blackPawn2, posBlack2);

        assertTrue(pawn.canMove(new Move(pawnWhitePos,posBlack1)));
        assertTrue(pawn.canMove(new Move(pawnWhitePos,posBlack2)));
    }

    @Test
    public void cantTakePiece() throws InvalidAttributeValueException {
        board.cleanBoard();
        Position posWhite = new Position(4,1);
        Position posBlack1 = new Position(4,2);
        Position posBlack2 = new Position(5,1);
        Position posBlack3 = new Position(4,0);

        Pawn whitePawn = new Pawn(Color.WHITE, board);
        Pawn blackPawn1 = new Pawn(Color.BLACK,board);
        Pawn blackPawn2 = new Pawn(Color.BLACK,board);
        Pawn blackPawn3 = new Pawn(Color.BLACK,board);

        board.addPieceInBoard(whitePawn, posWhite);
        board.addPieceInBoard(blackPawn1, posBlack1);
        board.addPieceInBoard(blackPawn2, posBlack2);
        board.addPieceInBoard(blackPawn3, posBlack3);

        assertFalse(whitePawn.canMove(new Move(posWhite,posBlack1)));
        assertFalse(whitePawn.canMove(new Move(posWhite,posBlack2)));
        assertFalse(whitePawn.canMove(new Move(posWhite,posBlack3)));
    }
}
