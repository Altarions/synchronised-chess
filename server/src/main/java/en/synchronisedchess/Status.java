package en.synchronisedchess;

public enum Status {
    WAITING,
    IN_PROGRESS,
    WHITE_WIN,
    BLACK_WIN
}