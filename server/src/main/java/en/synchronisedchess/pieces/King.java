package en.synchronisedchess.pieces;
import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;

public class King extends Piece {
    public King(Color color, BoardManager board) {
        super(color, board, (color == Color.BLACK) ? "♚": "♔");
    }

    public boolean canMove(Move move) {
        int rangeX = Math.abs(move.to.col-move.from.col);
        int rangeY = Math.abs(move.to.row-move.from.row);

        return (this.board.canMoveTo(move.to, this.color) && (rangeX == 1 || rangeX == 0) && (rangeY == 1 || rangeY == 0));
    }
}
