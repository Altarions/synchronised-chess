package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;

public class Bishop extends Piece {
    public Bishop(Color color, BoardManager board) {
        super(color, board, (color == Color.BLACK) ? "♝": "♗");
    }

    public boolean canMove(Move move) {
        int deltaX = move.to.col - move.from.col;
        int deltaY = move.to.row - move.from.row;

        // Check if the bishop is going diagonally
        if (Math.abs(deltaX) == Math.abs(deltaY)) {
            int stepX = Integer.signum(deltaX);
            int stepY = Integer.signum(deltaY);

            // Check if there is no obstruction in the way
            for (int i = 1; i < Math.abs(deltaX); i++) {
                Position position = new Position(move.from.col + i * stepX, move.from.row + i * stepY);

                if (this.board.getPieceFrom(position) != null) {
                    return false;
                }
            }
            return this.board.canMoveTo(move.to, this.color);
        }
        return false;
    }
}
