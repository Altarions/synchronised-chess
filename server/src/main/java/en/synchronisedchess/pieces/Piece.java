package en.synchronisedchess.pieces;


import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;

public abstract class Piece {
    final public Color color;
    final protected BoardManager board;
    final public String pieceCharacter;

    public Piece(Color color, BoardManager board, String pieceCharacter) {
        this.color = color;
        this.board = board;
        this.pieceCharacter = pieceCharacter;
    }

    public abstract boolean canMove(Move move);
}
