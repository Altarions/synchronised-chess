package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;

public class Pawn extends Piece {
    public Pawn(Color color, BoardManager board) {
        super(color, board, (color == Color.BLACK) ? "♟": "♙");
    }

    public boolean canMove(Move move) {
        int rangeX = Math.abs(move.to.col-move.from.col);
        //int rangeY = Math.abs(to.y-from.y);

        int direction;
        if(this.color == Color.WHITE) {
            direction = 1;
        } else {
            direction = -1;
        }

        /* TODO: tidy */
        return this.board.canMoveTo(move.to, this.color) && (
            /* Diagonal kill */ (rangeX == 1 && move.to.row-move.from.row == direction && this.board.getPieceFrom(move.to).color != this.color)
            || 
            (this.board.getPieceFrom(move.to) == null &&
                ((/* Two Steps */  move.to.row-move.from.row == 2*direction && this.board.getPieceFrom(new Position(move.to.col, move.to.row-direction))  == null && (move.from.row == 1 && this.color == Color.WHITE || move.from.row == 6 && this.color == Color.BLACK) && move.from.col-move.to.col == 0 )
                || /* One Step */ (move.to.row-move.from.row == direction && move.from.col - move.to.col == 0))
            )
        );

    }
}
