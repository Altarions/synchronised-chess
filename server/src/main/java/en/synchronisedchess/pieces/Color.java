package en.synchronisedchess.pieces;

public enum Color {
    BLACK, WHITE
}
