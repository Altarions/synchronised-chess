package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;

public class Queen extends Piece {
    public Queen(Color color, BoardManager board) {
        super(color, board, (color == Color.BLACK) ? "♛": "♕");
    }

    public boolean canMove(Move move) {
        int deltaX = move.to.col - move.from.col;
        int deltaY = move.to.row - move.from.row;

        // Check if the queen is going diagonally or in a straight line
        if (Math.abs(deltaX) == Math.abs(deltaY) || deltaX == 0 || deltaY == 0) {

            int stepX = (deltaX == 0)? 0 : Integer.signum(deltaX);
            int stepY = (deltaY == 0)? 0 : Integer.signum(deltaY);

            // Check if there is no obstruction in the way
            for (int i = 1; i < Math.max(Math.abs(deltaX),Math.abs(deltaY)); i++) {
                Position position = new Position(move.from.col + i * stepX, move.from.row + i * stepY);

                if (this.board.getPieceFrom(position) != null) {
                    return false;
                }
            }
            return this.board.canMoveTo(move.to, this.color);
        }
        return false;
    }
}
