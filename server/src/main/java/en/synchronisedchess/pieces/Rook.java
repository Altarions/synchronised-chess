package en.synchronisedchess.pieces;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;

public class Rook extends Piece {
    public Rook(Color color, BoardManager board) {
        super(color, board, (color == Color.BLACK) ? "♜": "♖");
    }

    public boolean canMove(Move move) {
        if (!board.canMoveTo(move.to,this.color)) return false;
        if (move.from.col == move.to.col) {
            return pathClearX(move.from, move.to);
        }else {
            if (move.from.row == move.to.row) {
                return pathClearY(move.from, move.to);
            } else {
                return false;
            }
        }
    }

    private boolean pathClearY(Position from, Position to) {
        int vector = Integer.signum(to.col-from.col);

        for(int i = from.col+vector; i !=to.col; i+=vector) {
            if(board.getPieceFrom(new Position(i,to.row)) != null){
                return false;
            }
        }
        return true;
    }

    private boolean pathClearX(Position from, Position to) {
        int vector = Integer.signum(to.row-from.row);

        for(int i = from.row+vector; i !=to.row; i+=vector) {
            if(board.getPieceFrom(new Position(to.col,i)) != null){
                return false;
            }
        }
        return true;
    }
}