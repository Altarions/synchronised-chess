package en.synchronisedchess;

import en.synchronisedchess.board.BoardManager;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.BoardManager.MoveConsequence;
import en.synchronisedchess.players.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Game {
    private BoardManager board;
    private Map<UUID, Player> players;
    private Status gameStatus;

    public Game(){
        this.board = new BoardManager();
        players = new HashMap<>();
        this.gameStatus = Status.WAITING;
    }


    /*public ArrayList<Player> getPlayers() {
        return players;
    }*/

    public Player getPlayer(UUID uid) {
        return players.get(uid);
    }

    public int  getNumberOfPlayers() {
        return this.players.size();
    }

    public void addPlayers(UUID uid, Player player) throws GameException {
        if(gameStatus.equals(Status.WAITING) && players.size() < 2){
            this.players.put(uid, player);
            System.out.println("Un joueur a rejoint la partie");
            if(players.size() == 2){
                
                // Skipping the ready state, starting instantly.
                // this.gameStatus = Status.READY;

                this.gameStatus = Status.IN_PROGRESS;

                System.out.println("La partie est READY");
            }
        } else {
            throw new GameException("Game was started or finished, you can't add a player !");
        }
    }

    public BoardManager getBoardManager() {
        return board;
    }

    public void move(Move move, Player player) throws GameException {
        if(gameStatus.equals(Status.IN_PROGRESS)){
            MoveConsequence mq = board.bufferMove(move, player);
            /*if(mq == MoveConsequence.DID_MOVE) {
                System.out.println("performed move : " + move.toString());
            }*/
            
            switch(mq) {
                case DID_MOVE:
                    System.out.println("performed move : " + move.toString());
                    break;
                case DIDNT_MOVE:
                    System.out.println("didn't perform move");
                    break;
                case WHITE_WIN:
                    System.out.println("performed move : " + move.toString());
                    System.out.println("White Wins");
                    this.gameStatus = Status.WHITE_WIN;
                    restartAfterCooldown();
                    break;
                case BLACK_WIN:
                    System.out.println("performed move : " + move.toString());
                    System.out.println("Black Wins");
                    this.gameStatus = Status.BLACK_WIN;
                    restartAfterCooldown();
                    break;
            }
        } else {
            throw new GameException("You can't move if the game was not "+Status.IN_PROGRESS+", currently : "+this.gameStatus);
        }
    }

    public void restartAfterCooldown() {
        new java.util.Timer().schedule( 
            new java.util.TimerTask() {
                @Override
                public void run() {
                    board = new BoardManager();
                    gameStatus = Status.IN_PROGRESS;
                }
            }, 
            5000 
        );
    }

    public Status getGameStatus(){
        return this.gameStatus;
    }
}
