package en.synchronisedchess.players;
import java.util.UUID;
import en.synchronisedchess.pieces.Color;

public class Player {


    private final Color color;
    private String name;

    public Player(Color color){
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
