package en.synchronisedchess;

/**
 * Represents an error happening in the execution of the Game.
 * This error can be of any kind.
 */
public class GameException extends Exception {

        public GameException(String message) {
            super(message);
        }

}
