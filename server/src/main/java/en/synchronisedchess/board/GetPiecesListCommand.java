package en.synchronisedchess.board;

import en.synchronisedchess.board.BoardBuffer.AsyncCallback;

public class GetPiecesListCommand implements Command {
    private BoardManager board;
    private AsyncCallback<String> callback;

    public GetPiecesListCommand(BoardManager board, AsyncCallback<String> callback) {
        this.board = board;
        this.callback = callback;
    }

    @Override
    public void run() {
        try {
            String result = board.getPiecesList();
            callback.onSuccess(result);
        } catch (Exception e) {
            callback.onError(e);
        }
    }

    @Override
    public String execute() {
        // This method is part of the Command interface, but the actual execution is handled in the run method.
        return null;
    }
}