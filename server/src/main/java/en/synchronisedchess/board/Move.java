package en.synchronisedchess.board;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Move {
    public Position from;
    public Position to;


    @JsonCreator
    public Move(@JsonProperty("from") Position from, @JsonProperty("to") Position to) {
        this.from = from;
        this.to = to;
    }

    public String toString() {
        return "Move(from(col:" + this.from.col + ",row:" + this.from.row + "),to(col:" + this.to.col + ",row:" + this.to.row+"))";
    }
}