package en.synchronisedchess.board;

public interface Command extends Runnable {
    String execute();
}

