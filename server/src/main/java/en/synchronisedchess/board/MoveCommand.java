package en.synchronisedchess.board;

import en.synchronisedchess.board.BoardBuffer.AsyncCallback;
import en.synchronisedchess.board.BoardManager.MoveConsequence;


import en.synchronisedchess.players.Player;

public class MoveCommand implements Command {
    private BoardManager board;
    private AsyncCallback<MoveConsequence> callback;
    private Move move;
    private Player player;

    public MoveCommand(BoardManager board, AsyncCallback<MoveConsequence> callback, Move move, Player player) {
        this.board = board;
        this.callback = callback;
        this.move = move;
        this.player = player;
    }

    @Override
    public void run() {
        try {
            MoveConsequence result = board.move(this.move, this.player);
            callback.onSuccess(result);
        } catch (Exception e) {
            callback.onError(e);
        }
    }

    @Override
    public String execute() {
        // This method is part of the Command interface, but the actual execution is handled in the run method.
        return null;
    }
}