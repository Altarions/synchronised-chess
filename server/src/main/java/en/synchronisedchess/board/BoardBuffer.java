package en.synchronisedchess.board;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

class BoardBuffer {
    private final BlockingQueue<Command> queue;
    private final ExecutorService executor;

    public BoardBuffer() {
        this.queue = new LinkedBlockingQueue<>();
        this.executor = Executors.newSingleThreadExecutor();

        Thread consumerThread = new Thread(() -> {
            this.processQueue();
        });

        consumerThread.start();
    }

    public void bufferExecuteCommand(Command command) {
        try {
            queue.put(command);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    private void processQueue() {
        while (true) {
            try {
                Command command = this.queue.take();
                // Execute the command
                executor.execute(() -> command.run());
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public interface AsyncCallback<T> {
        void onSuccess(T result);
        void onError(Exception e);
    }
}