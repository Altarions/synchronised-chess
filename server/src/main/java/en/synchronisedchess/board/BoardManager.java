package en.synchronisedchess.board;


import en.synchronisedchess.GameException;
import en.synchronisedchess.board.BoardBuffer.AsyncCallback;
import en.synchronisedchess.players.Player;
import en.synchronisedchess.pieces.*;


import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.management.InvalidAttributeValueException;


public class BoardManager {
    private Piece[][] board;
    private BoardBuffer buffer;

    public BoardManager() {
        this.buffer = new BoardBuffer();
        this.board = new Piece[8][8];
        this.initiatePieces();
    }

    private void initiatePieces() {
        for(int x = 0; x <= 7; x++) {
            board[1][x] = new Pawn(Color.WHITE, this);
            board[6][x] = new Pawn(Color.BLACK, this);
        }

        board[0][0] = new Rook(Color.WHITE, this);
        board[0][1] = new Knight(Color.WHITE, this);
        board[0][2] = new Bishop(Color.WHITE, this);
        board[0][3] = new Queen(Color.WHITE, this);
        board[0][4] = new King(Color.WHITE, this);
        board[0][5] = new Bishop(Color.WHITE, this);
        board[0][6] = new Knight(Color.WHITE, this);
        board[0][7] = new Rook(Color.WHITE, this);



        board[7][0] = new Rook(Color.BLACK, this);
        board[7][1] = new Knight(Color.BLACK, this);
        board[7][2] = new Bishop(Color.BLACK, this);
        board[7][3] = new Queen(Color.BLACK, this);
        board[7][4] = new King(Color.BLACK, this);
        board[7][5] = new Bishop(Color.BLACK, this);
        board[7][6] = new Knight(Color.BLACK, this);
        board[7][7] = new Rook(Color.BLACK, this);

    }

    public String bufferGetPiecesList() {
        CompletableFuture<String> resultFuture = new CompletableFuture<>();

        AsyncCallback<String> callback = new AsyncCallback<String>() {
            @Override
            public void onSuccess(String result) {
                resultFuture.complete(result);
            }

            @Override
            public void onError(Exception e) {
                resultFuture.completeExceptionally(e);
            }
        };

        GetPiecesListCommand getPiecesListCommand = new GetPiecesListCommand(this, callback);
        buffer.bufferExecuteCommand(getPiecesListCommand);

        try {
            // Wait for the asynchronous result
            return resultFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            // Handle exceptions as needed
            e.printStackTrace();
            return "Error occurred while getting pieces list.";
        }

    }

    public String getPiecesList() {
        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("[\n");
    
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[row].length; col++) {
                Piece piece = this.getPieceFrom(new Position(col, row));
                if (piece != null) {
                    if (jsonBuilder.length() > 2) {
                        jsonBuilder.append(",\n");
                    }
                    jsonBuilder.append("{\"col\":" + col + ",\"row\":" + row + ",\"piece\":\"" + piece.pieceCharacter + "\",\"color\":\"" + piece.color + "\"}");
                }
            }
        }
    
        jsonBuilder.append("\n]");
    
        return jsonBuilder.toString();
    }


    public Piece getPieceFrom(Position position) {
        if(this.isPositionValid(position)) {
            return board[position.row][position.col];
        } else {
            return null;
        }
    }

    public boolean isPositionValid(Position pos) {
        return (pos.col >= 0 && pos.col <= 7 && pos.row >= 0 && pos.row <= 7);
    }

    public boolean canMoveTo(Position pos, Color color) {
        Piece pieceFrom = getPieceFrom(pos);
        return isPositionValid(pos) && (pieceFrom == null || pieceFrom.color != color);
    }

    // move this elsewhere
    public enum MoveConsequence {
        WHITE_WIN,
        BLACK_WIN,
        DID_MOVE,
        DIDNT_MOVE
    }

    public MoveConsequence bufferMove(Move move, Player player) {
        CompletableFuture<MoveConsequence> resultFuture = new CompletableFuture<>();

        AsyncCallback<MoveConsequence> callback = new AsyncCallback<MoveConsequence>() {
            @Override
            public void onSuccess(MoveConsequence result) {
                resultFuture.complete(result);
            }

            @Override
            public void onError(Exception e) {
                resultFuture.completeExceptionally(e);
            }
        };

        MoveCommand moveCommand = new MoveCommand(this, callback, move, player);
        buffer.bufferExecuteCommand(moveCommand);

        try {
            // Wait for the asynchronous result
            return resultFuture.get();
        } catch (InterruptedException | ExecutionException e) {
            // Handle exceptions as needed
            e.printStackTrace();
            // throw exception ?
            return MoveConsequence.DIDNT_MOVE;
        }

    }

    public MoveConsequence move(Move move, Player player) throws GameException {
        Piece piece = board[move.from.row][move.from.col];

        if (piece != null && !player.getColor().equals(piece.color)){
            throw new GameException("You didn't select of of your pieces !");
        }

        if (this.isPositionValid(move.from) && piece != null) {

            if(piece.canMove(move)) {
                // Winning Condition
                boolean didWin = false;
                if(board[move.to.row][move.to.col] instanceof King) {
                    didWin = true;
                }

                if(piece instanceof Pawn && ((piece.color == Color.BLACK && move.to.row == 0) || piece.color == Color.WHITE && move.to.row == 7)) {
                    // Promotion to Queen
                    board[move.to.row][move.to.col] = new Queen(piece.color, this);
                } else {
                    board[move.to.row][move.to.col] = piece;
                }
                
                board[move.from.row][move.from.col] = null;

                return (didWin ? (piece.color == Color.BLACK ? MoveConsequence.BLACK_WIN : MoveConsequence.WHITE_WIN ) : MoveConsequence.DID_MOVE );
            }
        }
        return MoveConsequence.DIDNT_MOVE;
    }

    public void addPieceInBoard(Piece piece, Position to) throws InvalidAttributeValueException {
        if (getPieceFrom(to) == null) {
            board[to.row][to.col] = piece;
        }else{
            throw new InvalidAttributeValueException("You cannot place a piece on a square that is already occup !");
        }
    }

    public void cleanBoard(){
        this.board = new Piece[8][8];
    }
    public void resetBoard(){
        this.board = new Piece[8][8];
        this.initiatePieces();
    }
    
    public Piece[][] getBoard(){
        return this.board;
    }
}
