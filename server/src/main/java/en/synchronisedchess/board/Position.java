package en.synchronisedchess.board;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Position {
    public final int row;
    public final int col;

    @JsonCreator
    public Position(@JsonProperty("col") int col, @JsonProperty("row") int row) {
        this.row = row;
        this.col = col;
    }

}
