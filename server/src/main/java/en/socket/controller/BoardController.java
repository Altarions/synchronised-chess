package en.socket.controller;

import en.synchronisedchess.Status;
import en.synchronisedchess.board.Move;
import en.synchronisedchess.board.Position;
import en.synchronisedchess.pieces.Color;
import en.synchronisedchess.players.Player;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Controller;

import en.socket.models.BoardResponse;
import en.synchronisedchess.GameException;
import en.synchronisedchess.Game;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.security.Principal;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Controller
public class BoardController {

    private Game game;
    private final Map<String, WebSocketSession> sessions = new ConcurrentHashMap<>();



    // Send to all subscriber of the /receive/test --> @SendTo("/receive/test")
    @MessageMapping("/join") // when a request is made to that mapping
    @SendToUser("/topic/join") // send only to that one subscriber
    public String joinGame(Principal principal) throws GameException {

        if(game == null) game = new Game();

        // principal is null right now
        // System.out.println("Received private board request: " + principal.getName());
        if(game.getNumberOfPlayers() < 2) {
            Color playerColor = (game.getNumberOfPlayers() == 0) ? Color.WHITE : Color.BLACK;
            UUID uid = UUID.randomUUID();
            Player player = new Player(playerColor);
            
            try {
                game.addPlayers(uid, player);
            }
            catch(Exception e) {
                System.out.println(e.toString());
            }
        
            // return game.getGameStatus();
            return "{\"playing\": " + true + ",\"color\": \"" + playerColor + "\", \"uid\": \"" + uid + "\"}";
        }
        return "{\"playing\": " + false + "}";
        
    }

    /* 
     si je comprend bien on a besoin de:
     1) une requête qui t'ajoute a la game et te donne la couleur
     2) subscribe au plateau (qui te dis aussi si la game a commencé)
     3 envoyer des déplacement
    */

    @MessageMapping("/movement")
    public void receiveMove(@Payload String messageJson, Principal principal) throws GameException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            JsonNode rootNode = objectMapper.readTree(messageJson);

            Move move = objectMapper.treeToValue(rootNode.get("move"), Move.class);

            UUID uuid = UUID.fromString(rootNode.get("uuid").asText());
            
            // for now, forcing white player
            Player player = game.getPlayer(uuid);

            if(player != null) {
                game.move(move, player);
            }
            
        } catch (Exception e) {
            e.printStackTrace(); // Handle the exception according to your needs
        }
    }

    // Method to associate WebSocket sessions with user identifiers
    @EventListener
    public void handleWebSocketConnectListener(SessionConnectedEvent event) {

    }


    @MessageMapping("/board") // when a request is made to that mapping
    @SendToUser("/topic/board") // send only to that one subscriber
    public String sendBoard(Principal principal) throws GameException {
        return "{\"board\": " + game.getBoardManager().bufferGetPiecesList() + ", \"status\": \"" + game.getGameStatus() + "\"}";
    }
}
