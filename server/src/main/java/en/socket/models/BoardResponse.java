package en.socket.models;


import en.synchronisedchess.pieces.Piece;

public class BoardResponse {

    public Piece[][] pieces;

    public BoardResponse(Piece[][] pieces) {
        this.pieces = pieces;
    }
}
